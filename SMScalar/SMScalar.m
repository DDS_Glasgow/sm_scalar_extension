

(* Wolfram Language Raw Program *)

(*-------------------------------SM extended with a real scalar singlet with near conformal symmetry------------------------------------*)


	(*				F+- forms  			*)
	
FPM[h_, \[CurlyPhi]_,sign_] := (3 \[Lambda]1 + \[Lambda]2)/4 h^2 + (3 \[Lambda]3 + \[Lambda]2)/4 \[CurlyPhi]^2 + 
	sign*(Sqrt[((3 \[Lambda]1 - \[Lambda]2)/4 h^2 - (3 \[Lambda]3 - \[Lambda]2)/4 \[CurlyPhi]^2)^2 + \[Lambda]2^2 \[CurlyPhi]^2 h^2])
	

	
(*					Dimmless Potential			*)

VeffDimmless[h_, \[CurlyPhi]_] := 
 1/4 \[Lambda]1 h^4 + 1/2 \[Lambda]2 h^2 \[CurlyPhi]^2 + 
  1/4 \[Lambda]3 \[CurlyPhi]^4 + 
  3/(256 \[Pi]^2) (\[Lambda]1 h^2 + \[Lambda]2 \[CurlyPhi]^2)^2 Log[\
\[Lambda]1 h^2 + \[Lambda]2 \[CurlyPhi]^2] + 
  1/(64 \[Pi]^2) (FPM[h, \[CurlyPhi], +1])^2 Log[
    FPM[h, \[CurlyPhi], +1]] + 
  1/(64 \[Pi]^2) (FPM[h, \[CurlyPhi], -1])^2 Log[
    FPM[h, \[CurlyPhi], -1]] - 6/(32 \[Pi]^2) gt^4 h^4 Log[h^2] - 
  1/(32 \[Pi]^2) gM^4 \[CurlyPhi]^4 Log[\[CurlyPhi]^2]
  
  
  
  		(*			Real potential i.e. with included nu    *)
  		
  		
 VeffReal[h_, \[CurlyPhi]_] := 
 1/4 \[Lambda]1 h^4 + 1/2 \[Lambda]2 h^2 \[CurlyPhi]^2 + 
  1/4 \[Lambda]3 \[CurlyPhi]^4 + 
  3/(256 \[Pi]^2) (\[Lambda]1 h^2 + \[Lambda]2 \[CurlyPhi]^2)^2 Log[(\
\[Lambda]1 h^2 + \[Lambda]2 \[CurlyPhi]^2)/\[Nu]^2] + 
  1/(64 \[Pi]^2) (FPM[h, \[CurlyPhi], +1])^2 Log[
    FPM[h, \[CurlyPhi], +1]/\[Nu]^2] + 
  1/(64 \[Pi]^2) (FPM[h, \[CurlyPhi], -1])^2 Log[
    FPM[h, \[CurlyPhi], -1]/\[Nu]^2] - 6/(32 \[Pi]^2) gt^4 h^4 Log[h^2/\[Nu]^2] - 
  1/(32 \[Pi]^2) gM^4 \[CurlyPhi]^4 Log[\[CurlyPhi]^2/\[Nu]^2]
  
  
 
 
 		(*		2nd iteration of the mass function   *)
 		
 		
massesFunction2[Hsol_, \[Phi]sol_, repList_] := 
 Module[{Vaux}, Vaux[H_, \[Phi]_] := VeffReal[H, \[Phi]];
  Hderiv2 = Simplify[D[Vaux[H, \[Phi]], {H, 2}] /. repList];
  \[Phi]deriv2 = 
   Simplify[D[Vaux[H, \[Phi]], {\[Phi], 2}] /. repList];
  H\[Phi]deriv2 = 
   Simplify[D[D[Vaux[H, \[Phi]], H], \[Phi]] /. repList];
  matrixA = {{Hderiv2, 
     H\[Phi]deriv2}, {H\[Phi]deriv2, \[Phi]deriv2}};
  eigenVecA = Eigenvectors[matrixA];
  diagA = 
   Simplify[
    Inverse[Transpose[eigenVecA]].matrixA.Transpose[eigenVecA]];
  auxList = {diagA, eigenVecA};
  Return[auxList]]
  
  
    		(*			Real potential i.e. with included nu    *)
  		
  		
 VeffReal[h_, \[CurlyPhi]_] := 
 1/4 \[Lambda]1 h^4 + 1/2 \[Lambda]2 h^2 \[CurlyPhi]^2 + 
  1/4 \[Lambda]3 \[CurlyPhi]^4 + 
  3/(256 \[Pi]^2) (\[Lambda]1 h^2 + \[Lambda]2 \[CurlyPhi]^2)^2 Log[(\
\[Lambda]1 h^2 + \[Lambda]2 \[CurlyPhi]^2)/\[Nu]^2] + 
  1/(64 \[Pi]^2) (FPM[h, \[CurlyPhi], +1])^2 Log[
    FPM[h, \[CurlyPhi], +1]/\[Nu]^2] + 
  1/(64 \[Pi]^2) (FPM[h, \[CurlyPhi], -1])^2 Log[
    FPM[h, \[CurlyPhi], -1]/\[Nu]^2] - 6/(32 \[Pi]^2) gt^4 h^4 Log[h^2/\[Nu]^2] - 
  1/(32 \[Pi]^2) gM^4 \[CurlyPhi]^4 Log[\[CurlyPhi]^2/\[Nu]^2]

(*---------------------------------------------Massive scalar affixed by hand and non zero Higgs mu^2 term-------------------------------------------------------------*)

  		(*			Effective potential with massive decoupled scalar    *)
  		
  		
VeffMassive[h_, \[CurlyPhi]_] := -1/2 \[Mu]H2 h^2 + 
  1/4 \[Lambda]1 h^4 + 1/2 \[Lambda]2 h^2 \[CurlyPhi]^2 - 
  1/2 \[Mu]S2 \[CurlyPhi]^2 + 1/4 \[Lambda]3 \[CurlyPhi]^4 + 
  3/(256 \[Pi]^2) (-\[Mu]H2 + \[Lambda]1 h^2 + \[Lambda]2 \
\[CurlyPhi]^2)^2 Log[(-\[Mu]H2 + \[Lambda]1 h^2 + \[Lambda]2 \
\[CurlyPhi]^2)/\[Mu]^2] + 
  1/(64 \[Pi]^2) (FPMmassive[h, \[CurlyPhi], +1])^2 Log[
    FPMmassive[h, \[CurlyPhi], +1]/\[Mu]^2] + 
  1/(64 \[Pi]^2) (FPMmassive[h, \[CurlyPhi], -1])^2 Log[
    FPMmassive[h, \[CurlyPhi], -1]/\[Mu]^2] - 
  6/(32 \[Pi]^2) gt^4 h^4 Log[h^2/\[Mu]^2]
  
  
  
  		(*			Tree level potential with massive decoupled scalar	*)

VTreeHiggsScalar	[h_, \[CurlyPhi]_] := -1/2 \[Mu]H2 h^2 + 
  1/4 \[Lambda]1 h^4 + 1/2 \[Lambda]2 h^2 \[CurlyPhi]^2 - 
  1/2 \[Mu]S2 \[CurlyPhi]^2 + 1/4 \[Lambda]3 \[CurlyPhi]^4
  

  
  (*				F+- forms  for a massive scalar 			*)
	
FPMmassive[h_, \[CurlyPhi]_, 
  sign_] := (3 \[Lambda]1 + \[Lambda]2)/4 h^2 - 
  1/4*\[Mu]H2 + (3 \[Lambda]3 + \[Lambda]2)/4 \[CurlyPhi]^2 - 
  1/4*\[Mu]S2 + 
  sign*(Sqrt[((3 \[Lambda]1 - \[Lambda]2)/4 h^2 - 
         1/4*\[Mu]H2 - (3 \[Lambda]3 - \[Lambda]2)/4 \[CurlyPhi]^2 + 
         1/4 *\[Mu]S2)^2 + \[Lambda]2^2 \[CurlyPhi]^2 h^2])
         

	
  