(*	Local  Variables are:
	H		-	Higgs field after SSB, with H^2= Phi^dagger Phi
	phi		-	Scalar extension coupling to Higgs field and neutrinos
	lambda1	-	Higgs quartic coupling
	lambda2	-	Scalar-Higgs coupling
	lambda3	-	Scalar quartic coupling	

	
	Global Variables are:
	gt		-	Top coupling
	gM		-	Neutrino coupling
	\[nu]		-	Mass renormalization scale *)





(* F+ / F- integrals *)

FPlus[H_, phi_, lambda1_, lambda2_, lambda3_]:=
	(1/4)*(3*lambda1 + lambda2)*((H)^2)	+	(1/4)(3*lambda3 + lambda2)*((phi)^2) +
	Sqrt[((1/4)*(3*lambda1 - lambda2)*((H)^2)	-	(1/4)(3*lambda3 - lambda2)*((phi)^2))^2 +
	(lambda2^2)*(phi^2)*(H^2)]
		

FMinus[H_, phi_, lambda1_, lambda2_, lambda3_]:=
	(1/4)*(3*lambda1 + lambda2)*((H)^2)	+	(1/4)(3*lambda3 + lambda2)*((phi)^2) -
	Sqrt[((1/4)*(3*lambda1 - lambda2)*((H)^2)	-	(1/4)(3*lambda3 - lambda2)*((phi)^2))^2 +
	(lambda2^2)*(phi^2)*(H^2)]




(*	Effective potential function fot the SM scalar extension     *)

Veff[H_, \[Phi]_, \[Lambda]1_, \[Lambda]2_, \[Lambda]3_] := 
 Module[{FP, FM}, 
  	FP[H1_, \[Phi]1_] := 
   FPlus[H1, \[Phi]1, \[Lambda]1, \[Lambda]2, \[Lambda]3];
  	FM[H1_, \[Phi]1_] := 
   FMinus[H1, \[Phi]1, \[Lambda]1, \[Lambda]2, \[Lambda]3];
  	(\[Lambda]1 *H^4)/4 + (\[Lambda]2 *H^2 \[Phi]^2)/
   2 + (\[Lambda]3*\[Phi]^4)/4 + 
   3/(256 \[Pi]^2) (\[Lambda]1*
       H^2 + \[Lambda]2*\[Phi]^2)^2 Log[(\[Lambda]1*
         H^2 + \[Lambda]2*\[Phi]^2)/\[Nu]^2] + 
   1/(64 \[Pi]^2) (FP[H, \[Phi]])^2 Log[FP[H, \[Phi]]/\[Nu]^2] +  
   1/(64 \[Pi]^2) (FM[H, \[Phi]])^2 Log[FM[H, \[Phi]]/\[Nu]^2] - 
   6/(32 \[Pi]^2) gt^4 H^4 Log[H^2/\[Nu]^2] - 
   1/(32 \[Pi]^2) gM^4 \[Phi]^4 Log[\[Phi]^2/\[Nu]^2]
  ]



Veff2[H_, \[Phi]_, \[Lambda]1_, \[Lambda]2_, \[Lambda]3_, gt_, gM_] := 
 Module[{FP, FM}, 
  	FP[H1_, \[Phi]1_] := 
   FPlus[H1, \[Phi]1, \[Lambda]1, \[Lambda]2, \[Lambda]3];
  	FM[H1_, \[Phi]1_] := 
   FMinus[H1, \[Phi]1, \[Lambda]1, \[Lambda]2, \[Lambda]3];
  	(\[Lambda]1 *H^4)/4 + (\[Lambda]2 *H^2 \[Phi]^2)/
   2 + (\[Lambda]3*\[Phi]^4)/4 + 
   3/(256 \[Pi]^2) (\[Lambda]1*
       H^2 + \[Lambda]2*\[Phi]^2)^2 Log[(\[Lambda]1*
         H^2 + \[Lambda]2*\[Phi]^2)] + 
   1/(64 \[Pi]^2) (FP[H, \[Phi]])^2 Log[FP[H, \[Phi]]] +  
   1/(64 \[Pi]^2) (FM[H, \[Phi]])^2 Log[FM[H, \[Phi]]] - 
   6/(32 \[Pi]^2) gt^4 H^4 Log[H^2] - 
   1/(32 \[Pi]^2) gM^4 \[Phi]^4 Log[\[Phi]^2]
  ]





(*	Function that finds the masses of the Higgs and scalar after mixing
	by diagonalizing the mass matrix for the Higgs / Scalar				 *)

massesFunction[ 
  Hsol_, \[Phi]sol_, \[Lambda]1_, \[Lambda]2_, \[Lambda]3_] := 
 Module[ {Vaux},
  
  Vaux[H2_, \[Phi]2_] := 
   Veff[H2, \[Phi]2, \[Lambda]1, \[Lambda]2, \[Lambda]3 ];
  (*Vaux2=Vaux[H2, \[Phi]2_]/.{\[Nu]\[Rule]\[Nu]val};*)
  
  Hderiv2 = 
   Simplify[
    D[Vaux[H2, \[Phi]2], {H2, 2}] /. {H2 -> 
       Hsol, \[Phi]2 -> \[Phi]sol}];
  \[Phi]deriv2 = 
   Simplify[
    D[Vaux[H2, \[Phi]2], {\[Phi]2, 2}] /. {H2 -> 
       Hsol, \[Phi]2 -> \[Phi]sol}];
  H\[Phi]deriv2 = 
   Simplify[
    D[D[Vaux[H2, \[Phi]2], H2], \[Phi]2] /. {H2 -> 
       Hsol, \[Phi]2 -> \[Phi]sol}];
  
  matrixA = {  {Hderiv2, H\[Phi]deriv2}, 
     		{H\[Phi]deriv2, \[Phi]deriv2}} /. {\[Nu] -> \[Nu]val};
  
  eigenVecA = Eigenvectors[matrixA];
  diagA = 
   Simplify[
    Inverse[Transpose[eigenVecA]]. matrixA . Transpose[eigenVecA] ];
  auxList={diagA, eigenVecA};
  Return[auxList]
  
  ]

(*					Effective couplings RGEs         *)

y1RGE[\[Mu]_, y1_, y2_, x_] := \[Mu] * \!\(
\*SubscriptBox[\(\[PartialD]\), \(\[Mu]\)]\(y1[\[Mu]]\)\) == 
  3/2 y1[\[Mu]]^2 + 1/8 y2[\[Mu]]^2 - 
   6 x[\[Mu]]^2;      											 (*\[Lambda]1 RGE*)

y2RGE[\[Mu]_, y1_, y2_, y3_] := \[Mu]*\!\(
\*SubscriptBox[\(\[PartialD]\), \(\[Mu]\)]\(y2[\[Mu]]\)\) == 
  3/8 y2[\[Mu]] (2 y1[\[Mu]] + y3[\[Mu]] + 
     4/3 y2[\[Mu]]);      										 (*\[Lambda]2 RGE*)

y3RGE[\[Mu]_, y3_, y2_, u_] := \[Mu]*\!\(
\*SubscriptBox[\(\[PartialD]\), \(\[Mu]\)]\(y3[\[Mu]]\)\) == 
  9/8 y3[\[Mu]]^2 + 1/2 y2[\[Mu]]^2 - 
   u[\[Mu]]^2;       											(*\[Lambda]3 RGE*)


xRGE[\[Mu]_, x_, z_] := \[Mu]*\!\(
\*SubscriptBox[\(\[PartialD]\), \(\[Mu]\)]\(x[\[Mu]]\)\) == 
  9/4 x[\[Mu]]^2 - 4 x[\[Mu]]* z[\[Mu]];    					(* Top Yukawa RGE  *)

uRGE[\[Mu]_, u_] := \[Mu]*\!\(
\*SubscriptBox[\(\[PartialD]\), \(\[Mu]\)]\(u[\[Mu]]\)\) == 
  3/4 u[\[Mu]]^2;                          						 (*Neutrino Mixing Yukawa RGE*)
zRGE2[\[Mu]_, z_] := \[Mu]*\!\(
\*SubscriptBox[\(\[PartialD]\), \(\[Mu]\)]\(z[\[Mu]]\)\) == - 7/
   2 (z[\[Mu]])^2;                  							 (* Strong force RGE  *)
	
	
	
	
(*	See "Conformal Symetry and the Standard Model" by Meissner and Nicolai for details on potentials and functions used *)
